
import { SkSelectImpl }  from '../../sk-select/src/impl/sk-select-impl.js';

export class JquerySkSelect extends SkSelectImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'select';
    }

    get optionsEl() {
        if (! this._optionsEl) {
            this._optionsEl = this.comp.el.querySelector('.ui-selectmenu-menu');
        }
        return this._optionsEl;
    }

    get optionsListEl() {
        return this.optionsEl.querySelector('ul');
    }

    get dropdownEl() {
        if (! this._dropdownEl) {
            this._dropdownEl = this.comp.el.querySelector('.ui-selectmenu-button');
        }
        return this._dropdownEl;
    }

    set dropdownEl(el) {
        this._dropdownEl = el;
    }

    get selectedLabelEl() {
        if (! this._selectedLabelEl) {
            this._selectedLabelEl = this.comp.el.querySelector('.ui-selectmenu-text');
        }
        return this._selectedLabelEl;
    }

    set selectedLabelEl(el) {
        this._selectedLabelEl = el;
    }

    get popupEl() {
        return this.optionsEl;
    }

    get subEls() {
        return [ 'optionsEl', 'optionsListEl', 'dropdownEl', 'selectedLabelEl' ];
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }

    get btnMultiselectEl() {
        return this.comp.el.querySelector('button.ui-multiselect');
    }

    fmtMultiLabel() {
        let selectedNumber = this.comp.selectedValues.length;
        let optionsTotal = Object.keys(this.options).length;
        if (this.comp.selectedValues.length > 0) {
            return `${this.comp.selectedValues.slice(0, 3).join(', ')}${this.comp.selectedValues.length > 3 ? '..': ''}`;
        } else {
            return '--';
        }
    }

    onMouseLeave(event) {
        this.toggleSelection();
    }

    renderMultiMode(isMultiselect) {
        if (isMultiselect) {
            this.btnMultiselectEl.style.display = 'block';
            this.dropdownEl.style.display = 'none';
            let buttonLabel = this.btnMultiselectEl.querySelector('.sk-select-filter-label');
            buttonLabel.innerHTML = this.fmtMultiLabel();
            this.renderOptions();
            if (! this.leaveHandler) {
                this.optionsEl.onmouseleave = function(event) {
                    this.onMouseLeave(event);
                }.bind(this);
            }
        } else {
            this.btnMultiselectEl.style.display = 'none';
            this.dropdownEl.style.display = 'inline-block';
        }
    }

    restoreState(state) {
        //this.body.innerHTML = '';
        //this.body.insertAdjacentHTML('beforeend', state.contentsState);
        this.body.style.display = 'none'; // hide original select element
        this.optionsListEl.innerHTML = '';
        if (this.comp.hasAttribute('multiple')) {
            this.renderOptions();
            this.renderMultiMode(true);
            this.optionsEl.style.display = 'none';
        } else {
            this.renderOptions();
            this.renderMultiMode(false);
            if (this.comp.el.host.value) {
                let optionFound = false;
                for (let key of Object.keys(this.options)) {
                    let option = this.options[key];
                    let value = option.dataset.value || option.value || option.innerHTML
                    if (value === this.comp.el.host.value) {
                        this.selectOption(this.options[key]);
                        optionFound = true;
                    }
                }
                if (! optionFound) {
                    this.selectNoOption();
                }
            } else {
                this.selectNoOption();
                //this.selectOption();
            }
            this.toggleSelection(true);
        }
        this.bindEvents();
    }

    onClick(event) {
        this.comp.callPluginHook('onEventStart', event);
        let menuItemEl = this.findSelectionItem(event.path);
        if (menuItemEl && menuItemEl.tagName === 'LI' && menuItemEl.classList.contains('ui-selectmenu-optgroup')) { // optgroup click
            return false;
        }
        let target = event.originalTarget || event.srcElement || event.target; // firefox and ie
        if (menuItemEl || target.classList.contains('ui-menu-item') || target.classList.contains('ui-menu-item-wrapper')) {
            let selectedOption = menuItemEl || target;
            this.selectOption(selectedOption, event);
            if (! this.comp.hasAttribute('multiple')) {
                this.toggleSelection();
            }
        } else {
            if (this.isClickedOnDropdown(event)) {
                this.toggleSelection();
            }
        }
        this.comp.callPluginHook('onEventEnd', event);
    }

    toggleSelection(forceOpen) {
        let open = this.dropdownEl.classList.contains('ui-selectmenu-button-open');
        if (open || forceOpen) {
            this.optionsEl.style.display = 'none';
            this.dropdownEl.classList.remove('ui-selectmenu-button-open');
            this.comp.removeAttribute('open');
        } else {
            let box = this.dropdownEl.getBoundingClientRect();
            if (this.comp.hasAttribute('multiple')) {
                box = this.btnMultiselectEl.getBoundingClientRect();
            }
            this.optionsEl.style.display = 'block';
            this.optionsEl.style.width = (box.width - 2) + 'px';
            this.optionsEl.style.left = box.left + 'px';
            this.optionsEl.style.top = (box.bottom) + 'px';
            this.optionsEl.style.position = 'fixed';
            this.dropdownEl.classList.add('ui-selectmenu-button-open');
            this.comp.setAttribute('open', '');
        }
    }

    bindEvents() {
        super.bindEvents();
        if (this.clickHandle) {
            this.comp.removeEventListener('click', this.clickHandle);
        }
        this.clickHandle = this.onClick.bind(this);
        this.comp.addEventListener('click', this.clickHandle);
        this.bindFitPopup();
    }


    unbindEvents() {
        super.unbindEvents();
        if (this.clickHandle) {
            this.comp.removeEventListener('click', this.clickHandle);
        }
        if (this.onScrollHandler) {
            window.removeEventListener('scroll', this.onScrollHandler);
            delete this.onScrollHandler;
        }
    }


    renderOptions() {
        let num = 1;
        this.options = {};
        let optgroups = this.comp.querySelectorAll('optgroup') || this.optgroups;
        if (optgroups && optgroups.length > 0) {
            if (! this.optgroups) {
                this.optgroups = optgroups;
                for (let optgroup of this.optgroups) {
                    let label = optgroup.getAttribute('label') ? optgroup.getAttribute('label') : '';
                    this.optionsListEl.insertAdjacentHTML('beforeend', `
                    <li class="ui-selectmenu-optgroup ui-menu-divider">${label}</li>
                `);
                    let groupOptions = optgroup.querySelectorAll('option');
                    for (let option of groupOptions) {
                        this.renderOption(option, num);
                        this.options['option-' + num] = option;
                        num++;
                    }
                }
            }
        } else {
            let options = this.comp.querySelectorAll('option');

            for (let option of options) {
                this.renderOption(option, num);
                this.options['option-' + num] = option;
                num++;

            }
        }
    }

    renderOption(option, num) {
        let value = option.dataset.value || option.value || option.innerHTML;
        let title = option.innerHTML || value;
        if (this.comp.hasAttribute('multiple')) {
            this.optionsListEl.insertAdjacentHTML('beforeend', `
                    <li class="ui-menu-item sk-select-multi-item" data-value="${value}">
                        <input type="checkbox" ${this.comp.isSelected(value) ? 'checked' : ''} />
                        <span id="ui-id-${num}" tabindex="-1" role="option" class="ui-menu-item-wrapper">${title}</span>
                    </li>
                `);
        } else {
            this.optionsListEl.insertAdjacentHTML('beforeend', `
                    <li class="ui-menu-item" data-value="${value}">
                        <span id="ui-id-${num}" tabindex="-1" role="option" class="ui-menu-item-wrapper">${title}</span>
                    </li>
                `);
        }
        //option.parentElement.removeChild(option);

    }

    findSelectionItem(path) {
        if (! path) return null;
        for (let item of path) {
            if (item.tagName === 'LI') {
                return item;
            }
        }
    }

    selectOption(option, event) {
        if (this.comp.hasAttribute('multiple')) {
            let item = option;
            if (event && event.target.path) {
                let item = this.findSelectionItem(event.target.path);
            }
            let value = item.dataset.value !== undefined ? item.dataset.value : option.value || option.innerHTML;
            let checkbox = option.parentElement.querySelector(`li[data-value=${value}] input[type=checkbox]`);
            if (! checkbox.hasAttribute('checked')) {
                this.comp.selectValue(value);
                checkbox.setAttribute('checked', '');
            } else {
                this.comp.deSelectValue(value);
                checkbox.removeAttribute('checked');
            }
            this.renderMultiMode(true);
        } else {
            if (!option) {
                //option = this.options['option-1'];
                this.selectedLabelEl.innerHTML = '--';
                //this.comp.removeAttribute('value');
            } else {
                this.comp.setAttribute('value', (option.tagName === 'OPTION' && option.value !== undefined) ? option.value : option.dataset.value || option.innerText);
                this.comp.dispatchEvent(new CustomEvent('change', { target: this.comp, bubbles: true, composed: true }));
                this.comp.dispatchEvent(new CustomEvent('skchange', { target: this.comp, bubbles: true, composed: true }));

                this.selectedLabelEl.innerHTML = '';
                this.selectedLabelEl.insertAdjacentHTML('beforeend', option.innerText);
                this.comp.setAttribute('value', (option.tagName === 'OPTION' && option.value !== undefined) ? option.value : option.dataset.value || option.innerText);
            }


        }
    }

    selectNoOption() {
        this.selectedLabelEl.innerHTML = '';
        this.selectedLabelEl.insertAdjacentHTML('beforeend', '--');
    }

    disable() {
        this.dropdownEl.classList.add('ui-state-disabled');
    }

    enable() {
        this.dropdownEl.classList.remove('ui-state-disabled');
    }

    showInvalid() {
        super.showInvalid();
        this.dropdownEl.classList.add('form-invalid');
    }

    showValid() {
        super.showValid();
        this.dropdownEl.classList.remove('form-invalid');
    }

    focus() {
        this.btnMultiselectEl.focus();
    }
}
